"use strict";
const express = require("express");
const bodyParser = require("body-parser");
const descriptioncourse = require("./courses/courses");

var cors = require("cors");

const courses = [
  {
    id: 1,
    courseName: "Java Script"
  },
  {
    id: 2,
    courseName: "Type Script"
  },
  {
    id: 3,
    courseName: "NodeJs"
  },
  {
    id: 4,
    courseName: "Angular"
  },
  {
    id: 5,
    courseName: "DataBase"
  },
  {
    id: 6,
    courseName: "React Native"
  }
];

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

var MongoClient = require("mongodb").MongoClient;
var url = "mongodb://localhost:27017/";

//Import the endpoints.
const Login = require("./src/Login");
const Register = require("./src/Register");
const UserCourses = require("./src/UserCourses");
const EnrolledCourses = require("./src/EnrolledCourses");
const CoursesDescription = require("./src/CourseDescription");
const RatingCourse = require("./src/RatingCourse");
const AverageRating = require("./src/AverageRating");
const CourseComments = require("./src/CourseComments");
const CommentedCourse = require("./src/CommentedCourse")

let login = new Login(MongoClient, url);
let register = new Register(MongoClient, url);
let usercourses = new UserCourses(MongoClient, url, courses);
let enrolledcourses = new EnrolledCourses(MongoClient, url);
let coursesdescription = new CoursesDescription(descriptioncourse);
let ratingcourse = new RatingCourse(MongoClient,url);
let totalrating = new AverageRating(MongoClient,url);
let coursecomments = new CourseComments(MongoClient,url);
let commentedcourse = new CommentedCourse(MongoClient,url);
//********************************************************//
/*                   Get API                             */
//********************************************************//

// Get all courses
app.get("/allcourses", (req, res) => {
  res.status(200).send({
    success: "true",
    message: "successfully",
    courses: courses
  });
});

app.get("/description/:id", (req, res) => {
  console.log("des");
  coursesdescription.coursedescription1(req, res);
});

// Get all enrolled courses
app.get("/enrolledcourses/:id", (req, res) => {
  enrolledcourses.enrolledcourses(req, res);
});

// Get Total rating
app.get("/totalrating/:id", (req,res) => {
  totalrating.totalrating(req,res);
})

app.get("/commentedcourse/:id",(req,res) =>{
  commentedcourse.commentedcourse(req,res);
})
//********************************************************//
/*                    Post API                             */
//********************************************************//

//API for login
app.post("/login", (req, res) => {
  login.userlogin(req, res);
});

// API for signup
app.post("/signup", (req, res) => {
  register.usersignup(req, res);
});

app.post("/coursecomments/:id", (req,res) =>{
  coursecomments.commentcourse(req,res);
})
// To enroll course
app.post("/usercourse", (req, res) => {
  usercourses.enrollcourse(req, res);
});

app.post("/ratingcourse/:id", (req,res)=>{
  ratingcourse.ratingcourse(req,res);
})
const PORT = 5000;

app.listen(PORT, () => {
  console.log(`server running on port ${PORT}`);
});

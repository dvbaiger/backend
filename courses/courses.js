const courses = {
  1: {
    id: 1,
    introduction: "JavaScript is used to create client-side dynamic pages. JavaScript is an object-based scripting language which is lightweight and cross-platform. JavaScript is not a compiled language, but it is a translated language. The JavaScript Translator (embedded in the browser) is responsible for translating the JavaScript code for the web browser.",
    description: "JavaScript was introduced as a language for the client side. The development of Node.js has marked JavaScript as an emerging server-side technology too. However, as JavaScript code grows, it tends to get messier, making it difficult to maintain and reuse the code. Moreover, its failure to embrace the features of Object Orientation, strong type checking and compile-time error checks prevents JavaScript from succeeding at the enterprise level as a full-fledged server-side technology."
  },
  2:{
    id: 2,
    introduction: "TypeScript lets you write JavaScript the way you really want to. TypeScript is a typed superset of JavaScript that compiles to plain JavaScript. TypeScript is pure object oriented with classes, interfaces and statically typed like C# or Java. Mastering TypeScript can help programmers to write object-oriented programs and have them compiled to JavaScript",
     description: "JavaScript has seen a vast development in the last few years. It is the most versatile cross-platform language that is used to develop modern web applications. It can be used to develop both the client side of an application, with frameworks like Angular or React.js, as well as the server side, with frameworks such as Node.js. It is now being considered the most opted for language to build end-to-end applications. But, JavaScript was never meant for such large-scale application development." 
     },
  3:{
    id: 3,
    introduction:"The modern web application has really come a long way over the years with the introduction of many popular frameworks such as bootstrap, Angular JS, etc. All of these frameworks are based on the popular JavaScript framework. But when it came to developing server-based applications, there was a kind of void, and this is where Node.js came into the picture.",
    description: "Node.js is a highly efficient and scalable non-blocking I/O platform that was build on top of Google Chrome V8 engine and its ECMAScript.This means that most front-end JavaScript (another implementation of ECMAScript) objects, functions and methods are available in Node.js. Node.js is an open source, cross-platform runtime environment for developing server-side and networking applications. Node.js applications are written in JavaScript, and can be run within the Node.js runtime on OS X, Microsoft Windows, and Linux"
  },
  4: {
    id: 4,
    introduction: "Angular 7 is a JavaScript (actually a TypeScript based open-source full-stack web application) framework which makes you able to create reactive Single Page Applications (SPAs). Angular 7 is completely based on components. It consists of several components which forms a tree structure with parent and child components. Angular's versions beyond 2+ are generally known as Angular only",
    description: "Angular is a JavaScript framework which makes you able to create reactive Single Page Applications (SPAs). This is a leading front-end development framework which is regularly updated by Angular team of Google.Angular 7 is completely based on components. It consists of several components forming a tree structure with parent and child components. it dynamically rewrites the current page rather than loading entire new pages from a server. That's the reason behind its reactive fast speed."
  },
  5: {
    id: 5,
    introduction: "The name indicates what the database is. A database is one of the important components for many applications and is used for storing a series of data in a single set. In other words, it is a group/package of information that is put in order so that it can be easily accessed, manage and update. n a database, even the smallest portion of information becomes the data",
    description: "Database management system is software that is used to manage the database. DBMS  includes all topics of DBMS such as introduction, ER model, keys, relational model, join operation, SQL, functional dependency, transaction, concurrency control, etc.The database is a collection of inter-related data which is used to retrieve, insert and delete the data efficiently. It is also used to organize the data in the form of a table, schema, views, and reports, etc."
  },
  6: {
    id: 6,
    introduction: "Native Android and iOS development are quite different and can be expensive – first, the language itself is quite different and second, all the underlying API’s are different – the way of using the GPS is different, the way to create animations is different, the way you make network calls is different.",
    description: "React Native lets you build mobile apps using only JavaScript. It uses the same design as React, letting you compose a rich mobile UI from declarative components. With React Native, you don't build a mobile web app, an HTML5 app, or a hybrid app; you build a real mobile app that's indistinguishable from an app built using Objective-C or Java. React Native uses the same fundamental UI building blocks as regular iOS and Android apps. You just put those building blocks together using JavaScript and React."
  }
};
module.exports= courses;

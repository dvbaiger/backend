class Register {
  constructor(MongoClient, url) {
    this.MongoClient = MongoClient;
    this.url = url;
  }

  usersignup(req, res) {
    console.log(req.body);

    if (!req.body.name) {
      return res.status(400).send({
        success: "false",
        message: "fullname is required"
      });
    } else if (!req.body.email) {
      return res.status(400).send({
        success: "false",
        message: "email is required"
      });
    } else if (!req.body.password) {
      return res.status(400).send({
        success: "false",
        message: "password is required"
      });
    }

    this.MongoClient.connect(this.url, function(err, db) {
      if (err) throw err;
      var dbo = db.db("mydb");
      var myobj = {
        name: req.body.name,
        email: req.body.email,
        password: req.body.password
      };
      var query = { email: req.body.email };
      dbo
        .collection("users")
        .find(query)
        .toArray(function(err, result) {
          if (err) throw err;
          if (result.length > 0) {
            db.close();
            res.status(200).send({
              message: "user already exists"
            });
          } else {
            dbo.collection("users").insertOne(myobj, function(err, resCreate) {
              if (err) {
                res.status(400).send({
                  message: "unable to creat your account"
                });
              }
              db.close();
              res.status(200).send({
                success: "true",
                message: "Account  created "
              });
            });
          }
        });
    });
  }
}

module.exports = Register;

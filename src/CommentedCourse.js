class CommentedCourse {
  constructor(MongoClient, url) {
    this.MongoClient = MongoClient;
    this.url = url;
  }

  commentedcourse(req, res) {
    this.MongoClient.connect(this.url, function(err, db) {
      console.log("db connected", err);
      if (err) throw err;
      var dbo = db.db("mydb");
      console.log("userid", req.params);
      var query = { courseId: req.params.id };
      dbo
        .collection("coursecomment")
        .find(query)
        .toArray(function(err, result) {
          if (err) throw err;
          db.close();
          res.status(200).send({
            success: "true",
            message: "All comments",
            result: result
          });
        });
    });
  }
}

module.exports = CommentedCourse;

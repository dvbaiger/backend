class AverageRating {
  constructor(MongoClient, url) {
    this.MongoClient = MongoClient;
    this.url = url;
  }

  totalrating(req, res) {
    this.MongoClient.connect(this.url, function(err, db) {
      console.log("db connected", err);
      if (err) throw err;
      var dbo = db.db("mydb");
      console.log("userid", req.params);
      var query = { courseId: req.params.id };
      dbo
        .collection("rating")
        .find(query)
        .toArray(function(err, result) {
          var size = result.length;
          var averageRating = 0;
          for (var i = 0; i < size; i++) {
            averageRating += result[i].rating;
          }
          if (size === 0) {
            averageRating;
          } else {
            averageRating = averageRating / size;
          }
          console.log(" avg", averageRating);

          console.log(size);
          if (err) throw err;
          db.close();
          res.status(200).send({
            success: "true",
            message: "Average rating",
            averageRating: averageRating
          });
        });
    });
  }
}

module.exports = AverageRating;

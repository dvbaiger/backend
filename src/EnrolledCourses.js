class EnrolledCourses {
  constructor(MongoClient, url) {
    this.MongoClient = MongoClient;
    this.url = url;
  }

  enrolledcourses(req, res) {
    this.MongoClient.connect(this.url, function(err, db) {
      console.log("db connected", err);
      if (err) throw err;
      var dbo = db.db("mydb");
      console.log("userid", req.params);
      var query = { userId: req.params.id };
      dbo
        .collection("courses")
        .find(query)
        .toArray(function(err, result) {
          if (err) throw err;
          db.close();
          res.status(200).send({
            success: "true",
            message: "You have enrolled all this courses",
            result: result
          });
        });
    });
  }
}

module.exports = EnrolledCourses;

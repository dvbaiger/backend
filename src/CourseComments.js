class CourseComments {
  constructor(MongoClient, url) {
    this.MongoClient = MongoClient;
    this.url = url;
  }

  commentcourse(req, res) {
    var courseId = req.params.id;
    console.log("1", req.params);
    if (!courseId) {
      return res.status(400).send({
        success: "false",
        message: "courseId is required"
      });
    }

    this.MongoClient.connect(this.url, function(err, db) {
      console.log("db connected1", err);
      if (err) throw err;
      var dbo = db.db("mydb");
      console.log("comment1", req.body);
      const comment = {
        courseId: courseId,
        comment: req.body.comment,
        username: req.body.username
      };

      dbo
        .collection("coursecomment")
        .insertOne(comment, function(err, resCreate) {
          if (err) {
            res.status(400).send({
              message: "unable to enroll this course!"
            });
          }
          db.close();
          res.status(200).send({
            success: "true",
            message: " You have commented on this course",
            comment: comment
          });
        });
    });
  }
}

module.exports = CourseComments;

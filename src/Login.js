class Login {
  constructor(MongoClient, url) {
    this.MongoClient = MongoClient;
    this.url = url;
  }

  userlogin(req, res) {
    var email = req.body.email;
    var password = req.body.password;
    if (!email) {
      return res.status(400).send({
        success: "false",
        message: "email is required"
      });
    } else if (!password) {
      return res.status(400).send({
        success: "false",
        message: "password is require"
      });
    }

    this.MongoClient.connect(this.url, function(err, db) {
      console.log("db connected", err);
      if (err) throw err;
      var dbo = db.db("mydb");
      var query = { email: req.body.email, password: req.body.password };
      dbo
        .collection("users")
        .find(query)
        .toArray(function(err, result) {
          db.close();
          if (result.length == 0) {
            res.status(200).send({
              result: "Incorrect username or password"
            });
          } else {
            console.log(result);
            res.status(200).send({
              success: "true",
              message: "successfully logged in",
              result: result[0]
            });
          }
        });
    });
  }
}
module.exports = Login;

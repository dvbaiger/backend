class RatingCourse {
  constructor(MongoClient, url) {
    this.MongoClient = MongoClient;
    this.url = url;
  }

  ratingcourse(req, res) {
    var courseId = req.params.id;
    console.log(" body", req.params.id);
    if (!courseId) {
      return res.status(400).send({
        success: "false",
        message: "courseId is required"
      });
    }

    this.MongoClient.connect(this.url, function(err, db) {
      console.log("db connected", err);
      if (err) throw err;
      var dbo = db.db("mydb");
      const rating = {
        courseId: courseId,
        rating: req.body.rating
      };
      dbo.collection("rating").insertOne(rating, function(err, resCreate) {
        if (err) {
          res.status(400).send({
            message: "unable to enroll this course!"
          });
        }
        db.close();
        res.status(200).send({
          success: "true",
          message: "given rating successfully to this course ",
          rating: rating
        });
      });
    });
  }
}

module.exports = RatingCourse;

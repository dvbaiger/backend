class UserCourses {
  constructor(MongoClient, url, courses) {
    this.MongoClient = MongoClient;
    this.url = url;
    this.courses = courses;
  }
  enrollcourse(req, res) {
    var courseId = req.body.courseId;
    if (!courseId) {
      return res.status(400).send({
        success: "false",
        message: "courseId is required"
      });
    }

    this.MongoClient.connect(this.url, function(err, db) {
      console.log("db connected", err);
      if (err) throw err;
      var dbo = db.db("mydb");
      const course = {
        courseId: req.body.courseId,
        userId: req.body.userId,
        courseName: req.body.courseName,
        description: req.body.description
      };
      dbo.collection("courses").insertOne(course, function(err, resCreate) {
        if (err) {
          res.status(400).send({
            message: "unable to enroll this course!"
          });
        }
        db.close();
        res.status(200).send({
          success: "true",
          message: "Course Enrolled ",
          course
        });
      });
    });
  }
}

module.exports = UserCourses;

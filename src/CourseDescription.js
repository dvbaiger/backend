class CourseDescription {
  constructor(coursedescription) {
    this.coursedescription = coursedescription;
  }

  coursedescription1(req, res) {
    var id = req.params.id;
    res.status(200).send({
      success: "true",
      message: "get course details successfully",
      descriptioncourse: this.coursedescription[id]
    });
  }
}
module.exports = CourseDescription;
